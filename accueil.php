<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>

        .body{
            position: relative;
        }

        .logo{
            width: 550px;
            margin: 0 auto;
            padding-top: 100px;
        }

        .logo-img{
            width: 550px;

        }

        .button{
            border: solid 5px;
            width: 550px;
            height: 80px;
            border-radius: 85px;
            text-align: center;
            margin: 0 auto;
            padding-top: 10px;
            margin-top: 50px;
        }

        .button:hover{
        background: #FF1344;
        color: white;
        border: solid 5px;
        border-color: black;
    }
        .button-link{
            font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif; 
            font-size: 48px;
            text-decoration: none;
            color: black;
        }
        .button-link:hover{
            color: white;
        }

        .bubble{
            width: 740px;

        }

        .bubble-img{
            position: absolute;
            bottom:40%;
            right: 5%;
            z-index: 1;
            width: 740px;

        }

        .mariotta{
            height: 600px;

        }

        @keyframes from-bottom {
        from {bottom: -600px;}
        to {bottom: 0%;}
        }
        .mariotta-img{
            width: 600px;
            position: absolute;
            bottom:0%;
            right: calc(0% - 100px);
            animation-name: from-bottom;
            animation-duration: 4s;

        }

        .text{
            font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif; 
            font-size: 24px;
            position: absolute;
            bottom:50%;
            right:17.5%;
            text-align: center;

        }

    </style>

</head>
<body>
    <div class="logo">
        <img class="logo-img" src="logo.png" alt="logo">
    </div>
    <div class="button">
        <a class="button-link" href="qcm_test.php"> Commencez le test </a>
    </div>

    <div class="bubble">
        <img class="bubble-img" src="./bulle.svg" alt="bulle">
    </div>
    <div class="mariotta">
        <img class="mariotta-img" src="./mariotta.png" alt="">
    </div>
    
    <p class="text"> Testez vos <br> connaissances!</p>

</body>
</html>