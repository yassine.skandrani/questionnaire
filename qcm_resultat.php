<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Résultat QCM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./reset.css">
    <style>
.header{
    width:100%;
    padding: 75px 0px 75px;
    border: 3px solid;
    margin-bottom:20px;
    font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
    position: relative;
}
.title{
    text-align:center;
    font-size: 70px;
}

.mariotta{
    position: absolute;
    height: 230px;
    bottom: calc(0% - 2px);
    right: 10%;
}
.texte_score{
    text-align: center;
    margin: 0 auto;
    margin-top: 50px;
    font-size: 50px;
    text-transform: uppercase;
    font-family:Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
}
.graph{
    margin-top: -5px;
    justify-content: center;
    display: flex;
    width: 100%;
    padding-left: 60px;
}
</style>

</head>
<header class="header">
        <h1 class="title">QUIZ SEMANTIQUE</h1>
        <img class="mariotta" src="mariotta.png" alt="">
    </header>
</html>

<?php
$score=0;
foreach ($_POST as $key=>$reponse)
{
    if ($reponse == "true")
        {
        $score++;
        }
    else
        {
            
        }
}

echo ("<p class=\"texte_score\"> Votre score: ".$score."/15 </p>");
?>

<html>
<!--CODE POUR RÉALISER UN GRAPHIQUE DES RÉSULTATS, VIA GOOGLE CHARTS-->
<head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table
    function drawChart() {
        var good = <?php echo $score; ?>;
        var bad = <?php echo 15-$score; ?>;

    // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            ['Bonnes Réponses', good],
            ['Mauvaises Réponses', bad],
        ]);

    // Set chart options
        var options = {'title':'',
                        'width':900,
                        'height':700};

    // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, options);
    }
    </script>
</head>
<body>
    <div class="graph" id="chart_div"></div>
</body>
</html>