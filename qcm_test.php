<?php
$file = file("qcm.txt");
shuffle($file);
$file = array_slice($file,0,15);
?>

<!DOCTYPE html>
<html>
<style>
.header{
    width:100%;
    padding: 75px 0px 75px;
    border: 3px solid;
    margin-bottom:20px;
    font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
    position: relative;
}
.title{
    text-align:center;
    font-size: 70px;
}

.mariotta{
    position: absolute;
    height: 235px;
    bottom: calc(0% - 2px);
    right: 10%;
}
.container__question{
    width:100%;
    padding: 10px 0px 10px;
    border: 3px solid;
    font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
}

.question{
    font-size: 35px;
    padding-left: 50px;
}
.answer__container{
    margin:auto;
    width: 70%;
    display: flex;
    flex-wrap:wrap;
    justify-content:center;
    font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
}
.answer__box{
    width: 45%;
    background: white;
    border: 3px solid;
    border-color:#FF1344;
    padding: 100px 0px 100px;
    margin: 20px;
    cursor: pointer;
    font-size: 25px;
    text-align: center;
}

.answer__box:hover{
    background: #FF1344;
    color: white;
}
.submit{
    padding: 40px 150px;
    background:white;
    border: 2px solid;
    margin: 50px 0px 75px;
    border-color:#FF1344;
    font-size: 25px;
}
input[type=radio]{
    border: 0px;
    width: 20px;
    height: 20px;
}
.submit:hover{
    background: #FF1344;
    color: white;
}
</style>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>QCM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="main.js"></script>
    <link rel="stylesheet" href="./reset.css">
</head>

<body>
    <header class="header">
        <h1 class="title">QUIZ SEMANTIQUE</h1>
        <img class="mariotta" src="mariotta.png" alt="">
    </header>
    <form class="answer__container" action="qcm_resultat.php" method="post">
<?php
$nb = 1;
foreach($file as $line)
{
    $lines = explode("##",$line);
    echo "<br>";
    $question = $lines[0];
    $reponse[0] = $lines[1];
    $reponse[1] = $lines[2];
    $reponse[2] = $lines[3];
    $reponse[3] = $lines[4];
    //cursor: pointer;
    echo("<div class=\"container__question\"><h3 class=\"question\"><label class=\"question\">".$nb.". ".$question."</label></h3></div><br>");
    shuffle($reponse);
    for($i=0 ;$i<count($reponse);$i++)
    {
        $new_reponse1 = str_replace("(","",$reponse[$i]);
        $new_reponse1 = str_replace(")","",$new_reponse1);

        if (strpos($reponse[$i],"(") !== false)
        {
            $value = "true";
            
        }
        else
        {
            $value = "false";
        }
        echo("<div class=\"answer__box\"><input class=\"answer\" type=\"radio\" name=\"reponse".$nb."\" value=\"".$value."\">".$new_reponse1."</div><br>");
    }
    $nb++;
}
?>  
        <input class="submit" type="submit">
    </form>
</body>

</html>